"use strict";

var gulp = require('gulp'),
    plumber = require('gulp-plumber'),
    sourcemaps = require('gulp-sourcemaps'),
    imagemin = require('gulp-imagemin'),
    optipng = require('imagemin-optipng'),
    svgo = require('imagemin-svgo'),
    browserify = require('browserify'),
    babelify = require('babelify'),
    source = require('vinyl-source-stream'),
    buffer = require('vinyl-buffer'),
    uglify = require('gulp-uglify'),
    please = require('gulp-pleeease'),
    sync = require('browser-sync');

var notify = require('gulp-notify'),
    argv = require ('yargs').argv;

var tunnel = argv.tunnel;

var path = {
    src: {
        style: 'resources/assets/sass/app.scss',
        fonts: 'resources/assets/fonts/**/*',
        scripts: 'resources/assets/es6/app.es6',
        images: 'resources/assets/images/*',
        vendor: 'resources/assets/vendor/*/**',
        blade: 'resources/views/**/*.blade.php',
    },

    build: {
        style: 'public/css/',
        fonts: 'public/fonts/',
        scripts: 'public/js/',
        images: 'public/images/main/',
        vendor: 'public/vendor',
    },
};

var server = {
    proxy: 'stalcon.loc',
    host: 'localhost',
    port: 3000,
    online: false,
    tunnel: 'stalcon',
    open: false,
    files: ['css/app.min.css'],
};


gulp.task('styles', function(){
    gulp.src(path.src.style)
        .pipe(plumber({errorHandler: notify.onError('Error with styles: <%= error.message %>') }))
        .pipe(sourcemaps.init())
            .pipe(please({
                out: 'public/css/app.min.css',
                minifier: true,
                rem: false,
                autoprefixer: { browsers: ['> 1%', 'last 2 versions', 'Firefox ESR', 'Opera 12.1', 'ie 8', 'ie 9'] },
                import: {
                    path: 'resources/assets/vendor/',
                },
                sass: {
                    includePaths: [
                        'resources/assets/sass',
                    ]
                },
            }))
        .pipe(sourcemaps.write('/'))
        .pipe(gulp.dest('.'))
        .pipe(sync.stream());
});

gulp.task('scripts', function(){
    return browserify({
            entries: path.src.scripts,
            extensions: ['.es6'],
            debug: true
        })
        .transform(babelify, {
            presets: ['es2015'],
        })
        .bundle()
        .on('error', function(err){
            console.log(err.toString());
            this.emit("end");
        })
        .pipe(plumber({errorHandler: notify.onError('Error with scripts: <%= error.message %>') }))
        .pipe(source('app.min.js'))
        .pipe(gulp.dest(path.build.scripts))
        .pipe(sync.stream());
});

gulp.task('images', function(){
    gulp.src(path.src.images)
        .pipe(plumber({errorHandler: notify.onError('Error with images: <%= error.message %>') }))
        .pipe(imagemin({
            progressive: true,
            use: [optipng(), svgo()],
        }))
        .pipe(gulp.dest(path.build.images));
        sync.reload();
});

gulp.task('vendor', function(){
    gulp.src(path.src.vendor)
        .pipe(plumber({errorHandler: notify.onError('Error with vendor files: <%= error.message %>') }))
        .pipe(gulp.dest(path.build.vendor));
});

gulp.task('blade', function(){
    gulp.src(path.src.blade)
        .pipe(plumber({errorHandler: notify.onError('Error with blade templates: <%= error.message %>') }))
        .pipe(sync.stream());
});

gulp.task('sync', function(){
    if(tunnel)
        server.online = true;

    sync.init(server);
    gulp.watch('resources/assets/sass/**/*.scss', ['styles']);
    gulp.watch('resources/assets/es6/**/*.es6', ['scripts']);
    gulp.watch(path.src.vendor, ['vendor']);
    gulp.watch(path.src.blade, ['blade']);
    gulp.watch(path.src.images, ['images']);
});

gulp.task('tunnel', function(){
    server.online = true;
    sync.init(server);
});

gulp.task('build', ['styles', 'vendor', 'blade', 'images', 'scripts']);
gulp.task('default', ['build', 'sync']);
