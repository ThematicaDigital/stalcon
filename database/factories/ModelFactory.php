<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Models\Service::class, function (Faker\Generator $faker){
    return [
        'title' => $faker->sentence(rand(3,6)),
        'image' => $faker->imageUrl(1200, 600),
        'description' => $faker->realText(rand(300, 600)),
        'published' => rand(0,1),
    ];
});

$factory->define(App\Models\GalleryAlbum::class, function(Faker\Generator $faker){
    return [
        'title' => $faker->sentence(rand(3,6)),
        'description' => $faker->realText(rand(300, 600)),
        'cover' => $faker->imageUrl(800, 500),
        'published' => rand(0,1),
    ];
});

$factory->define(App\Models\GalleryItem::class, function(Faker\Generator $faker){
    return [
        'title' => $faker->sentence(rand(3,6)),
        'image' => $faker->imageUrl(800, 500),
        'published' => rand(0,1),
    ];
});

$factory->define(App\Models\Article::class, function(Faker\Generator $faker){
    return [
        'title' => $faker->sentence(rand(4, 6)),
        'image' => $faker->imageUrl(1200, 800),
        'text' => $faker->realText(rand(800, 1200), rand(3,5)),
        'published' => rand(0,1),
    ];
});

$factory->define(App\Models\NewsItem::class, function(Faker\Generator $faker){
    return [
        'title' => $faker->sentence(rand(4, 6)),
        'image' => $faker->imageUrl(1200, 800),
        'text' => $faker->realText(rand(800, 1200), rand(3,5)),
        'published' => rand(0,1),
    ];
});

$factory->define(App\Models\Slide::class, function(Faker\Generator $faker){
    return [
        'title' => $faker->sentence(rand(5,7)),
        'text' => $faker->realText(rand(80, 120)),
        'image' => $faker->imageUrl(1200, 500),
        'published' => rand(0,1),
    ];
});
