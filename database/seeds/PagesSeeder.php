<?php

use Illuminate\Database\Seeder;
use App\Models\Page;
use Faker\Factory as Faker;

class PagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pages')->truncate();

        $titles = ['О компании', 'Вакансии'];
        $faker = Faker::create('ru_RU');

        foreach ($titles as $key => $title) {
            if(0 == $key)
                $image = $faker->imageUrl(1200, 600);
            else
                $image = '';

            $page = new Page;
            $page->title = $title;
            $page->text = $faker->realText(rand(1200, 1500));
            $page->image = $image;
            $page->save();
        }
    }
}
