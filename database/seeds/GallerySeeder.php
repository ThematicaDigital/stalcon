<?php

use Illuminate\Database\Seeder;

class GallerySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('gallery_albums')->truncate();
        DB::table('gallery_items')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        factory(App\Models\GalleryAlbum::class, 20)->create()->each(function($album){
            $album->images()->saveMany(factory(App\Models\GalleryItem::class, rand(5,10))->make());
        });
    }
}
