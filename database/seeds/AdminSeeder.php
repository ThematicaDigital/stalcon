<?php

use Illuminate\Database\Seeder;
use SleepingOwl\AdminAuth\Entities\Administrator;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Administrator::truncate();

        $default = [
			'username' => 'admin',
			'password' => 'admin',
			'name'     => 'Администратор'
		];

		Administrator::create($default);
    }
}
