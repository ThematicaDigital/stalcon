<?php

use Illuminate\Database\Seeder;

class ServicesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('services')->truncate();

        factory(App\Models\Service::class, 15)->create()->each(function($service1){
            factory(App\Models\Service::class, 15)->make()->each(function($service2) use ($service1){
                $service2->parent = $service1->id;
                $service2->save();
            });
        });
    }
}
