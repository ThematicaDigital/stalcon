<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        File::cleanDirectory(public_path().'/images/uploads/tmp');

        $this->call(AdminSeeder::class);
        $this->call(ServicesSeeder::class);
        $this->call(GallerySeeder::class);
        $this->call(PagesSeeder::class);
        $this->call(ArticlesSeeder::class);
        $this->call(NewsSeeder::class);
        $this->call(SlidesSeeder::class);

        Model::reguard();
    }
}
