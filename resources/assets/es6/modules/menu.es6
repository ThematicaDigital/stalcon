import $ from 'jquery';

export default class
{
    constructor(active)
    {
        this.$active = $(active);

        this.bind();
    }

    bind()
    {
        this.$active.parents('li').addClass('active');
    }
}
