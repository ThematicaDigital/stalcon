import $ from 'jquery';

export default class
{
    constructor(button, options)
    {
        this.$button = $(button);
        this.$target = $(options.target);

        this.bind();
    }

    bind()
    {
        this.$button.click((e)=>{
            e.preventDefault();
            this.$target.toggle();
        });
    }
}
