import 'jcarousel';

export default class
{
    constructor(slider, controls)
    {
        this.$slider = $(slider);
        this.$next = $(controls.next);
        this.$prev = $(controls.prev);

        this.init();
    }

    init()
    {
        this.$slider.on('jcarousel:create jcarousel:reload', () => {
            var width = this.$slider.innerWidth();
            this.$slider.jcarousel('items').css('width', width + 'px');
        }).jcarousel({
            wrap: 'circular',
            transitions: true,
            animation: {
                duration: 500,
                easing: 'linear',
            },
        }).jcarouselAutoscroll({
            interval: 3000,
            target: '+=1',
            autostart: true,
        });

        this.$next.click((e)=>{
            e.preventDefault();
            this.$slider.jcarousel('scroll', '+=1');
        });

        this.$prev.click((e)=>{
            e.preventDefault();
            this.$slider.jcarousel('scroll', '-=1');
        });
    }
}
