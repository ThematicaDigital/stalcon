import $ from 'jquery';
import Slider from './modules/slider';
import Menu from './modules/menu';
import Hamburger from './modules/hamburger';

import 'uikit';
import 'uikit.lightbox';

$('.jcarousel').each((i, slider) => new Slider(slider, $(slider).data('controls')));
$('.tree-menu li.active').each((i, active) => new Menu(active));
$('.hamburger .button').each((i, button) => new Hamburger(button, $(button).data('target')));
