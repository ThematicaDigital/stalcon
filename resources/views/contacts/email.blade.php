<p>
    Сообщение с сайта 'Амурский завод металлических конструкций' от {{ $user['name'] }}:
    {{ $user['text'] }}
</p>

@if($user['phone'] || $user['email'])
    Связаться с пользователем можно используя:
    <ul>
        <li>Тел.: {{ $user['phone'] }}</li>
        <li>Email: <a href="mailto:{{ $user['email'] }}">{{ $user['email'] }}</a></li>
    </ul>
@endif
