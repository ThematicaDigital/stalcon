<div class="row">
    @if(count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if(session('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif
    {!! Form::open(['class'=>'']) !!}
        <div class="form-group">
            {!! Form::label('name', 'Ф.И.О.', ['class'=>'required']) !!}
            {!! Form::text('name', '', ['class'=>'form-control']) !!}
        </div>

        <div class="row">
            <div class="form-group col-md-6">
                {!! Form::label('email', 'Электронная почта', ['class'=>'required']) !!}
                {!! Form::text('email', '', ['class'=>'form-control']) !!}
            </div>

            <div class="form-group col-md-6">
                {!! Form::label('phone', 'Телефон', ['class'=>'required']) !!}
                {!! Form::text('phone', '', ['class'=>'form-control']) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('text', 'Задайте вопрос нашим специалистам', ['class'=>'required']) !!}
            {!! Form::textarea('text', '', ['class'=>'form-control', 'rows'=>4]) !!}
        </div>

        {{-- <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                {!! Recaptcha::render() !!}
            </div>
        </div> --}}

        <div class="form-group">
            {!! Form::submit('Отправить заявку', ['class'=>'btn purple-button']) !!}
        </div>
    {!! Form::close() !!}
</div>
