@extends('layout')

@section('content')
    <div class="content-box">
        <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=lp-_LiwlEMS7bK5RNgoQuzw1j4Ddz12a&width=100%&height=400&lang=ru_RU&sourceType=constructor&scroll=true"></script>
        <div class="contacts">
            <h1>Как нас найти</h1>
            <h4>СТАЛКОН</h4>
            <div class="item">
                <b>Адрес: </b>{{ config('contacts.address') }}
            </div>
            <div class="item">
                <b>Телефон: </b>{{ config('contacts.phone') }}
            </div>
            <div class="item">
                <b>Часы работы: </b>{{ config('contacts.workHours') }}
            </div>

            <div class="social">
                <p>Расскажи о нас в социальных сетях</p>
                @include('partials.social')
            </div>

            <div class="form">
                @include('contacts.form')
            </div>
        </div>
    </div>
@endsection
