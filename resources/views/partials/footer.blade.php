<div class="container-fluid">
    <div class="row">
        <footer>
            <div class="container">
                <div class="row hidden-xs">
                    <ul class="row menu main footer col-xs-12">
                        <li {{{ (Active::checkUri(['/']) ? 'class=active' : '') }}}><a href="{{ route('mainpage') }}">Главная</a></li>
                        <li {{{ (Active::checkUri(['o-kompanii']) ? 'class=active' : '') }}}><a href="{{ route('staticpage', 'o-kompanii') }}">О компании</a></li>
                        <li {{{ (Active::checkUriPattern(['services', 'services/*']) ? 'class=active' : '') }}}><a href="{{ route('services') }}">Услуги</a></li>
                        <li {{{ (Active::checkUriPattern(['gallery', 'gallery/*']) ? 'class=active' : '') }}}><a href="{{ route('gallery') }}">Наши работы</a></li>
                        <li {{{ (Active::checkUriPattern(['news', 'news/*']) ? 'class=active' : '') }}}><a href="{{ route('news') }}">Новости</a></li>
                        <li {{{ (Active::checkUriPattern(['articles', 'articles/*']) ? 'class=active' : '') }}}><a href="{{ route('articles') }}">Статьи</a></li>
                        <li {{{ (Active::checkUri(['vakansii']) ? 'class=active' : '') }}}><a href="{{ route('staticpage', 'vakansii') }}">Вакансии</a></li>
                    </ul>
                </div>

                <div class="row visible-xs col-xs-12">
                    <ul class="row menu main footer col-xs-6">
                        <li {{{ (Active::checkUri(['/']) ? 'class=active' : '') }}}><a href="{{ route('mainpage') }}">Главная</a></li>
                        <li {{{ (Active::checkUri(['o-kompanii']) ? 'class=active' : '') }}}><a href="{{ route('staticpage', 'o-kompanii') }}">О компании</a></li>
                        <li {{{ (Active::checkUriPattern(['services', 'services/*']) ? 'class=active' : '') }}}><a href="{{ route('services') }}">Услуги</a></li>
                    </ul>

                    <ul class="row menu main footer col-xs-6">
                        <li {{{ (Active::checkUriPattern(['gallery', 'gallery/*']) ? 'class=active' : '') }}}><a href="{{ route('gallery') }}">Наши работы</a></li>
                        <li {{{ (Active::checkUriPattern(['news', 'news/*']) ? 'class=active' : '') }}}><a href="{{ route('news') }}">Новости</a></li>
                        <li {{{ (Active::checkUriPattern(['articles', 'articles/*']) ? 'class=active' : '') }}}><a href="{{ route('articles') }}">Статьи</a></li>
                        <li {{{ (Active::checkUri(['vakansii']) ? 'class=active' : '') }}}><a href="{{ route('staticpage', 'vakansii') }}">Вакансии</a></li>
                    </ul>
                </div>

                <div class="row">
                    <div class="row col-xs-10 info">
                        Все права защищены, 2016<br>
                        Сталькон - производство металлических конструкций. Использование материалов сайта без согласования с владельцами ресурса преследуется в соответствии с Законодательством РФ
                    </div>
                </div>

                <div class="row social">
                    @include('partials.social')
                </div>

                <div class="up">
                    <a href="#top"></a>
                </div>
            </div>
        </footer>
    </div>
</div>
