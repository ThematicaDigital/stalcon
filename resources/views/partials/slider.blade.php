<div class="jcarousel" data-controls="{{ json_encode(['next'=>'#nextslide', 'prev'=>'#prevslide']) }}">
    <ul>
        @foreach($slider as $slide)
            <li style="background: url({{ asset($slide->image) }}) no-repeat center center / 100%;" class="slider-item">
                {{-- {!! Html::image($slide->image, '', ['class'=>'img-responsive']) !!} --}}
                <div class="caption">
                    <h4>
                        @if($slide->link)
                            <a href="/{{ $slide->link }}">{{ $slide->title }}</a>
                        @else
                            {{ $slide->title }}
                        @endif
                    </h4>
                    <span>
                        {!! $slide->text !!}
                    </span>
                </div>
            </li>
        @endforeach
    </ul>
    <div data-controls class="controls">
        <a id="prevslide" href="#">&lsaquo;</a>
        <a id="nextslide" href="#">&rsaquo;</a>
    </div>
</div>
