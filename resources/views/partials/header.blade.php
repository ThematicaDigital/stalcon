<div class="container-fluid">
    <div class="row">
        <header>
            <div class="top">
                <div class="container">
                    <div class="row">
                        <div class="logo col-xs-5 col-md-6">
                            <a class="pull-left" href="/">{!! Html::image('/images/main/logo.png') !!}</a>
                        </div>
                        <a name="top"></a>

                        <ul class="contacts col-xs-7 col-md-6 pull-right">
                            <li>{!! Html::image('/images/main/pin.svg') !!}<a href="{{ route('contacts') }}">Контакты</a></li>
                            <li>{!! Html::image('/images/main/phone.svg') !!}{{ config('contacts.phone') }}</li>
                            <li>{!! Html::image('/images/main/at.svg') !!}<a href="mailto:{{ config('contacts.email') }}">{{ config('contacts.email') }}</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="bottom">
                <div class="container">
                    <div class="row">
                        <div class="search pull-right col-md-4">
                            {{ Form::open(['url'=>route('search')]) }}
                                {{ Form::text('search', '' , ['placeholder'=>'Поиск...']) }}
                                {{ Form::submit('', ['class'=>'submit-search']) }}
                            {{ Form::close() }}
                        </div>

                        <ul class="menu main row col-md-9 col-lg-8">
                            <li {{{ (Active::checkUri(['/']) ? 'class=active' : '') }}}><a href="{{ route('mainpage') }}">Главная</a></li>
                            <li {{{ (Active::checkUri(['o-kompanii']) ? 'class=active' : '') }}}><a href="{{ route('staticpage', 'o-kompanii') }}">О компании</a></li>
                            <li {{{ (Active::checkUriPattern(['services', 'services/*']) ? 'class=active' : '') }}}><a href="{{ route('services') }}">Услуги</a></li>
                            <li {{{ (Active::checkUriPattern(['gallery','gallery/*']) ? 'class=active' : '') }}}><a href="{{ route('gallery') }}">Наши работы</a></li>
                            <li {{{ (Active::checkUriPattern(['news','news/*']) ? 'class=active' : '') }}}><a href="{{ route('news') }}">Новости</a></li>
                            <li {{{ (Active::checkUriPattern(['articles','articles/*']) ? 'class=active' : '') }}}><a href="{{ route('articles') }}">Статьи</a></li>
                            <li {{{ (Active::checkUri(['vakansii']) ? 'class=active' : '') }}}><a href="{{ route('staticpage', 'vakansii') }}">Вакансии</a></li>
                        </ul>

                        <div class="hamburger">
                            <a class="button" data-target="{{ json_encode(['target' => '[data-hamburger]']) }}"></a>
                            <ul data-hamburger class="menu">
                                <li {{{ (Active::checkUri(['/']) ? 'class=active' : '') }}}><a href="{{ route('mainpage') }}">Главная</a></li>
                                <li {{{ (Active::checkUri(['o-kompanii']) ? 'class=active' : '') }}}><a href="{{ route('staticpage', 'o-kompanii') }}">О компании</a></li>
                                <li {{{ (Active::checkUriPattern(['services', 'services/*']) ? 'class=active' : '') }}}><a href="{{ route('services') }}">Услуги</a></li>
                                <li {{{ (Active::checkUriPattern(['gallery','gallery/*']) ? 'class=active' : '') }}}><a href="{{ route('gallery') }}">Наши работы</a></li>
                                <li {{{ (Active::checkUriPattern(['news','news/*']) ? 'class=active' : '') }}}><a href="{{ route('news') }}">Новости</a></li>
                                <li {{{ (Active::checkUriPattern(['articles','articles/*']) ? 'class=active' : '') }}}><a href="{{ route('articles') }}">Статьи</a></li>
                                <li {{{ (Active::checkUri(['vakansii']) ? 'class=active' : '') }}}><a href="{{ route('staticpage', 'vakansii') }}">Вакансии</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </header>
    </div>
</div>
