<div class="col-xs-12 item">
    <div class="content-box">
        <div class="col-xs-4 image-container">
            <a href="{{ Request::url() }}/{{ $item->slug }}">{!! Html::image($item->image, '', ['class'=>'img-responsive']) !!}</a>
        </div>
        <div class="col-xs-8">
            <div class="row">
                <div class="col-xs-12">
                    <h3><a href="{{ Request::url() }}/{{ $item->slug }}">{{ $item->title }}</a></h3>
                    <span>
                        {!! str_limit($item->text, 110) !!}
                    </span>
                </div>
                <div class="col-xs-12 down">
                    <a href="{{ Request::url() }}/{{ $item->slug }}">Подробнее</a>
                    <span>{{ Date::parse($item->created_at)->format('j F Y') }}</span>
                </div>
            </div>
        </div>
    </div>
</div>
