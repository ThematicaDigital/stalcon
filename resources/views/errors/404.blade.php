@extends('layout')

@section('title')
    Ошибка - {{ env('DEFAULT_TITLE') }}
@endsection

@section('content')
    <h1>Страница не найдена</h1>
    Страница не найдена, попробуйте вернуться на <a href="/">главную страницу</a>
@endsection
