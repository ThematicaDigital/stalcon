@extends('layout')

@section('slider')
    @include('partials.slider')
@endsection

@section('content')
    @include('services.list')
    @include('pages.show', ['page' => $about])
@stop
