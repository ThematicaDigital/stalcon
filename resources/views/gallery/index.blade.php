@extends('layout')

@section('slider')
    @include('partials.slider')
@endsection

@section('content')
    <div class="gallery">
        <div class="row">
            @if(!$albums->isEmpty())
                @foreach($albums as $album)
                    <a href="{{ route('album', $album->slug) }}">
                        <div class="col-xs-12 col-md-6 item">
                            {!! Html::image($album->cover, '', ['class'=>'img-responsive', 'alt'=>$album->title]) !!}
                            <div class="col-xs-12">
                                <div class="caption col-xs-12">
                                    <b>Альбом:</b><br>
                                    {{ $album->title }}
                                </div>
                            </div>
                        </div>
                    </a>
                @endforeach
            @else
                <div class="col-xs-12">
                    В данный момент раздел пуст
                </div>
            @endif
        </div>
        <div class="text-right">
            {!! with(new App\Paginator\Paginator($albums))->render() !!}
        </div>
    </div>
@endsection
