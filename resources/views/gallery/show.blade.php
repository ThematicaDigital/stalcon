@extends('layout')

@section('content')
    @if(!$album->images->isEmpty())
        <div class="row album">
            @if($album->description)
                <div class="col-xs-12">
                    <div class="content-box">
                        <h1>{{ $album->title }}</h1>
                        {!! $album->description !!}
                    </div>
                </div>
            @endif

            @foreach($album->images as $image)
                <div class="col-md-4 col-xs-6">
                    <a data-uk-lightbox="{group: '{{{ $album->slug }}}'}" href="{{ asset($image->image) }}">
                        <div class="item" style="background: url({{ asset($image->image) }}) no-repeat center center/100%;"></div>
                    </a>
                </div>
            @endforeach
        </div>
    @endif
@endsection
