@extends('layout')

@section('slider')
    @include('partials.slider')
@endsection

@section('content')
    @if(!$articles->isEmpty())
        <div class="row articles">
            @each('partials.articles-list', $articles, 'item')
        </div>
        <div class="text-right">
            {!! with(new App\Paginator\Paginator($articles))->render() !!}
        </div>
    @else
        В данный момент раздел пуст
    @endif
@endsection
