<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <link rel='shortcut icon' type='image/x-icon' href='/favicon/favicon.ico' />
        {!! Meta::title() !!}
        {!! Meta::keywords() !!}
        {!! Meta::description() !!}
        {!! Html::style('css/app.min.css') !!}
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                @include('partials.header')
                <div class="container">
                    <div class="row">
                        <div class="row slider-box">
                            <div class="col-xs-12">
                                @yield('slider')
                            </div>
                        </div>
                        <div class="row main-content">
                            <div class="col-md-4">
                                @include('services.menu')
                                @include('news.last')
                            </div>
                            <div class="col-md-8">
                                @yield('content')
                            </div>
                        </div>
                    </div>
                </div>
                @include('partials.footer')
            </div>
        </div>

        {!! Html::script('js/app.min.js') !!}
    </body>
</html>
