@extends('layout')

@section('content')
    <div class="row services">
        @if($service)
            <div class="col-xs-12">
                {!! Html::image($service->image, '', ['class'=>'img-responsive']) !!}
                <div class="col-xs-12 service-box">
                    <h1>{{ $service->title }}</h1>
                    <span>
                        {!! $service->description !!}
                    </span>
                </div>
            </div>
        @endif

        <div class="col-xs-12">
            <div class="about">
                Сделать заказ или получить консультацию,можно: <br>
                по телефону <b>{{ config('contacts.phone') }}</b>, либо по электронной почте <a href="mailto:{{ config('contacts.email') }}">{{ config('contacts.email') }}</a>
            </div>
        </div>
    </div>
@endsection
