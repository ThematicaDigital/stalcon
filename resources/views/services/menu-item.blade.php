<li class="{{ (Active::checkUriPattern(['services/'.$service->slug]))?'active':'' }} {{ ($service->childrens->isEmpty())?'end':'' }}">
    <a href="/services/{{ $service->slug }}">{{ $service->title }}</a>
    @if(!$service->childrens->isEmpty())
        <ul class="menu">
            @each('services.menu-item', $service->childrens, 'service')
        </ul>
    @endif
</li>
