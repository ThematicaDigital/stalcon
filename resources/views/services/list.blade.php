@if(!$services->isEmpty())
    <div class="row">
        @foreach($services as $index => $service)
            <div class="col-xs-12 col-md-6 services">
                <a href="/services/{{ $service->slug }}">
                    <div class="image-container">
                        <div class="triangle"><p>+</p></div>
                        @if(!empty($service->image))
                            {!! Html::image($service->image) !!}
                        @endif
                    </div>
                </a>
                <div class="service-box">
                    <h3><a href="/services/{{ $service->slug }}">{{ $service->title }}</a></h3>
                    <span>
                        {!! str_limit($service->description, 150) !!}
                    </span>
                </div>
            </div>

            @if($index != 0 && $index % 2 == 1)
                <div class="clearfix"></div>
            @endif
        @endforeach
        @if(Active::checkUriPattern(['services','services/*']))
            <div class="text-right">
                {!! with(new App\Paginator\Paginator($services))->render() !!}
            </div>
        @endif
    </div>
@else
    В данный момент раздел пуст
@endif
