@if(!$servicesMenu->isEmpty())
    <div class="services-menu">
        <h3>Категории услуг</h3>
        <ul class="menu tree-menu">
            @each('services.menu-item', $servicesMenu, 'service')
        </ul>
    </div>
@endif
