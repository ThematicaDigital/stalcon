@extends('layout')

@section('slider')
    @include('partials.slider')
@endsection

@section('content')
    @include('services.list')
@endsection
