@if($page)
    <div class="row pages">
        <div class="col-xs-12">
            @if($page->image)
                {!! Html::image($page->image, '', ['class'=>'img-responsive']) !!}
            @endif
            <div class="content-box">
                <h1>{{ $page->title }}</h1>
                {!! $page->text !!}
            </div>
        </div>
    </div>
@endif
