@if(!$lastNews->isEmpty())
    <div class="lastNews">
        <h3><a href="/news">Новости</a></h3>
        @foreach($lastNews as $newsItem)
            <div class="item">
                <h4><a href="/news/{{ $newsItem->slug }}">{{ $newsItem->title }}</a></h4>
                <div class="text">
                    {!! str_limit($newsItem->text, 100) !!}
                </div>
                <div class="date">
                    {{ Date::parse($newsItem->created_at)->format('j F Y') }}
                </div>
            </div>
        @endforeach
    </div>
@endif
