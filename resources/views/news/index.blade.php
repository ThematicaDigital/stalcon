@extends('layout')

@section('slider')
    @include('partials.slider')
@endsection

@section('content')
    @if(!$news->isEmpty())
        <div class="row">
            <div class="col-xs-12 articles">
                @each('partials.articles-list', $news, 'item')
            </div>
            <div class="text-right">
                {!! with(new App\Paginator\Paginator($news))->render() !!}
            </div>
        </div>
    @else
        В данный момент раздел пуст
    @endif
@endsection
