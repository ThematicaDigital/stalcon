<?php

return [
    /**
     * Config for title meta field
     * 'prepend' - text before title
     * 'append' - text after title
     * 'separator' - separator between page title, prepend and append
     */
    'title' => [
        'prepend' => '',
        'append' => ' - Сталькон',
        'separator' => '',
        'default' => 'Сталькон',
    ],

    /**
     *
     */
    'description' => [
        'template' => '',
        'default' => 'Сталькон',
    ],

    /**
     *
     */
    'keywords' => [
        'template' => '',
        'default' => 'Сталькон',
    ],
];
