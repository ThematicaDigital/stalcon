<?php

return [
	/*
	 * Admin title
	 * Displays in page title and header
	 */
	'company'                  => env('COMPANY', 'Сталкон'),
	'phone'                    => env('CONTACT_PHONE', '(4162) 58-85-07'),
    'address'                  => env('ADDRESS', 'Театральная ул., 159, Благовещенск 675000'),
    'email'                    => env('CONTACT_EMAIL', 'pkpstalkon@mail.ru'),
    'workHours'                => env('WORK_HOURS', '09:00‐18:00'),
];
