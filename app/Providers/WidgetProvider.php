<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Http;
use App\Http\Composers\LastNews;
use App\Http\Composers\ServicesMenu;
use App\Http\Composers\About;
use App\Http\Composers\Slides;

class WidgetProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composers([
            LastNews::class => 'news.last',
            Slides::class => 'partials.slider',
            About::class => 'index.index',
            ServicesMenu::class => 'services.menu',
        ]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
