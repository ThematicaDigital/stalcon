<?php
namespace App\Paginator;

use Illuminate\Pagination\BootstrapThreePresenter;

class Paginator extends BootstrapThreePresenter {
    public function render()
    {
        if(!$this->hasPages())
            return '';

        return sprintf('<div class="custom-pagination"><ul>%s</ul><ul class="controls">%s %s</ul></div>',
            $this->getLinks(),
            $this->getPreviousButton(),
            $this->getNextButton()
        );
    }
}
