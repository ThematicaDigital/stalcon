<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Slide extends Model
{
    use \SleepingOwl\Admin\Traits\OrderableModel;

    public function scopePublished($query)
    {
        return $query->where('published', 1);
    }

    public function scopeOrdered($query)
    {
        return $query->orderBy('order', 'asc');
    }

    public function getOrderField()
    {
        return 'order';
    }
}
