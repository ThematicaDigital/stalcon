<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\GalleryAlbum;

class GalleryItem extends Model
{
    public function gallery()
    {
        return $this->belongsTo(GalleryAlbum::class, 'album_id');
    }
}
