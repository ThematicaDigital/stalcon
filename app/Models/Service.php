<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Service extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'title',
        'save_to'    => 'slug',
    ];

    public function childrens()
    {
        return $this->hasMany(Service::class, 'parent')->published()->ordered();
    }

    public function firstParent()
    {
        return $this->belongsTo(Service::class, 'parent');
    }

    public function scopePublished($query)
    {
        return $query->where('published', 1);
    }

    public function scopeOrdered($query)
    {
        return $query->orderBy('order', 'asc');
    }

    public function scopeRoot($query)
    {
        return $query->where('parent', 0);
    }

    public function scopeChildrensOf($query, $parentId)
    {
        return $query->where('parent', $parentId);
    }

    public function scopeSearch($query, $word)
    {
        return $query->where('title', 'like', '%'.$word.'%');
    }

    public function setParentAttribute($value)
    {
        if(!isset($value))
            $this->attributes['parent'] = 0;
        else
            $this->attributes['parent'] = $value;
    }
}
