<?php
namespace App\Http\Composers;

use Illuminate\View\View;
use App\Models\Service;

class ServicesMenu
{
    public function compose(View $view)
    {
        $services = Service::published()->root()->ordered()->get();
        $view->servicesMenu = $services;
    }
}
