<?php
namespace App\Http\Composers;

use Illuminate\View\View;
use App\Models\Slide;

class Slides
{
    public function compose(View $view)
    {
        $view->slider = Slide::published()->ordered()->get();
    }
}
