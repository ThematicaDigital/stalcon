<?php
namespace App\Http\Composers;

use Illuminate\View\View;
use App\Models\Page;

class About
{
    public function compose(View $view)
    {
        $view->about = Page::findBySlug('o-kompanii');
    }
}
