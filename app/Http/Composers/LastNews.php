<?php
namespace App\Http\Composers;

use Illuminate\View\View;
use App\Models\NewsItem;

class LastNews
{
    public function compose(View $view)
    {
        $view->lastNews = NewsItem::published()->last(2)->get();
    }
}
