<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Page;

use Meta;

class PagesController extends Controller
{
    public function index($slug)
    {
        $page = Page::findBySlugOrFail($slug);

        Meta::set('title', $page->title);
        return view('pages.index', [
            'page' => $page,
        ]);
    }
}
