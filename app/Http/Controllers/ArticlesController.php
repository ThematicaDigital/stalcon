<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Article;

use Meta;

class ArticlesController extends Controller
{
    public function index()
    {
        $articles = Article::published()->ordered()->paginate(10);

        Meta::set('title', 'Статьи');

        return view('articles.index', [
            'articles' => $articles,
        ]);
    }

    public function show($slug)
    {
        $article = Article::findBySlugOrFail($slug);
        Meta::set('title', $article->title);

        return view('pages.index', [
            'page' => $article,
        ]);
    }
}
