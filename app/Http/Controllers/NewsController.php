<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\NewsItem;

use Meta;

class NewsController extends Controller
{
    public function index()
    {
        $news = NewsItem::published()->ordered()->paginate(10);
        Meta::set('title', 'Новости');

        return view('news.index', [
            'news' => $news,
        ]);
    }

    public function show($slug)
    {
        $newsItem = NewsItem::findBySlugOrFail($slug);
        Meta::set('title', $newsItem->title);

        return view('pages.index', [
            'page' => $newsItem,
        ]);
    }
}
