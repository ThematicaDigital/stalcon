<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\ContactsFormRequest;
use App\Http\Controllers\Controller;

use Mail;
use Meta;

class ContactsController extends Controller
{
    public function index()
    {
        Meta::set('title', 'Контакты');
        return view('contacts.index');
    }

    public function send(ContactsFormRequest $request) {
        $user = $request->all();

        Mail::send('contacts.email', ['user' => $user], function($mail) use ($user){
            $mail->from($user['email'], 'Амурский завод металлических конструкций');
            $mail->to(config('contacts.email'))->subject('Вопрос с сайта "Амурский завод металлических конструкций"');
        });

        return redirect()->action('ContactsController@index')->withMessage('Ваше сообщение было отправлено');
    }
}
