<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\GalleryAlbum;

use Meta;

class GalleryController extends Controller
{
    public function index()
    {
        $albums = GalleryAlbum::published()->ordered()->paginate(10);
        Meta::set('title', 'Наши работы');

        return view('gallery.index', [
            'albums' => $albums,
        ]);
    }

    public function show($slug)
    {
        $album = GalleryAlbum::where('slug', $slug)->firstOrFail();
        Meta::set('title', $album->title);

        return view('gallery.show', [
            'album' => $album,
        ]);
    }
}
