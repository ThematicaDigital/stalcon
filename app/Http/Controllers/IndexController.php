<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Page;
use App\Models\Service;

use Meta;

class IndexController extends Controller
{
    public function index()
    {
        Meta::set('title', 'Главная');
        Meta::set('keywords', 'test1, test2, test3 ,test4');
        Meta::set('description', 'some test description for mainpage');

        $services = Service::published()->root()->ordered()->take(10)->get();

        return view('index.index', [
            'services' => $services,
        ]);
    }

    public function search(Request $request)
    {
        $word = $request->input('search');
        Meta::set('title', "Результаты поиска по {$word}");

        $services = Service::search($word)->published()->get();

        return view('services.index', [
            'services' => $services,
        ]);
    }
}
