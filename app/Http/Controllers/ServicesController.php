<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Service;

use Meta;

class ServicesController extends Controller
{
    public function index()
    {
        Meta::set('title', 'Услуги');

        $services = Service::published()->root()->ordered()->paginate(10);
        return view('services.index', [
            'services' => $services,
        ]);
    }

    public function show($slug)
    {
        $rootService = Service::findBySlugOrFail($slug);
        $services = Service::childrensOf($rootService->id)->published()->ordered()->paginate(10);

        Meta::set('title', $rootService->title);

        if($services->isEmpty())
        {
            return view('services.show', [
                'service' => $rootService,
            ]);
        }

        return view('services.index', [
            'services' => $services,
        ]);
    }
}
