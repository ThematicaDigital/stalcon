<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'IndexController@index')->name('mainpage');
    Route::post('/search', 'IndexController@search')->name('search');

    Route::group(['prefix'=>'/contacts'], function(){
        Route::get('/', 'ContactsController@index')->name('contacts');
        Route::post('/', 'ContactsController@send');
    });

    Route::group(['prefix'=>'/gallery'], function(){
        Route::get('/', 'GalleryController@index')->name('gallery');
        Route::get('/{category}', 'GalleryController@show')->name('album');
    });

    Route::group(['prefix'=>'/news'], function(){
        Route::get('/', 'NewsController@index')->name('news');
        Route::get('/{slug}', 'NewsController@show');
    });

    Route::group(['prefix'=>'/articles'], function(){
        Route::get('/', 'ArticlesController@index')->name('articles');
        Route::get('/{slug}', 'ArticlesController@show');
    });

    Route::group(['prefix'=>'/services'], function(){
        Route::get('/', 'ServicesController@index')->name('services');
        Route::get('/{slug}', 'ServicesController@show');
    });

    Route::get('/{slug}', 'PagesController@index')->name('staticpage')->where('slug', '(?!admin)[\w\d\-]+');
});
