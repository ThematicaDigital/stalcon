<?php
namespace App\Admin\FormItems;

use SleepingOwl\Admin\FormItems\NamedFormItem;

class Disabled extends NamedFormItem
{
    public function render()
    {
        $params = $this->getParams();

        return view('forms.disabled', [
            'params' => $params,
        ]);
    }
}
