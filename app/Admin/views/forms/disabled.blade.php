<div class="form-group">
    <label for="{{ $params['name'] }}">{{ $params['label'] }}</label>
    <input id="{{ $params['name'] }}" class="form-control" type="text" name="{{ $params['name'] }}" value="{{ $params['value'] }}" readonly>
</div>
