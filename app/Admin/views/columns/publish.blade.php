<td class="text-center">
    @if($value)
        <i class="fa fa-fw fa-check"></i>
    @else
        <i class="fa fa-fw fa-times"></i>
    @endif
</td>
