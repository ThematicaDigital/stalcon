<?php
namespace App\Admin\Columns;

use SleepingOwl\Admin\Columns\Column\NamedColumn;

class PhotosLink extends NamedColumn
{
    public function render()
    {
        return view('columns.photosLink', [
            'value' => $this->getValue($this->instance, $this->name()),
        ]);
    }
}
