<?php
namespace App\Admin\Columns;

use SleepingOwl\Admin\Columns\Column\NamedColumn;

class Publish extends NamedColumn
{
    public function render()
    {
        return view('columns.publish', [
            'value' => $this->getValue($this->instance, $this->name()),
        ]);
    }
}
