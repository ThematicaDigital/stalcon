<?php
namespace App\Admin\Columns;

use SleepingOwl\Admin\Columns\Column\NamedColumn;

class AlbumLink extends NamedColumn
{
    public function render()
    {
        return view('columns.albumLink', [
            'item' => $this->instance,
        ]);
    }
}
