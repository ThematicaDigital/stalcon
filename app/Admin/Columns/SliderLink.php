<?php
namespace App\Admin\Columns;

use SleepingOwl\Admin\Columns\Column\NamedColumn;

class SliderLink extends NamedColumn
{
    public function render()
    {
        return view('columns.sliderLink', [
            'item' => $this->instance,
        ]);
    }
}
