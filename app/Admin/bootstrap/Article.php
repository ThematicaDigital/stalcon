<?php
use App\Models\Article;

Admin::model(Article::class)->title('Статьи')->display(function ()
{
	$display = AdminDisplay::datatables();
	$display->attributes([
		'ordering' => false,
		// 'stateSave' => false,
	]);
	$display->scope('ordered');
	$display->columns([
		Column::string('id')->label('ID'),
		Column::string('title')->label('Заголовок'),
		Column::image('image')->label('Изображение'),
		Column::string('created_at')->label('Создан'),
		Column::publish('published')->label('Опубликовано'),
	]);
	return $display;
})->createAndEdit(function ()
{
	$form = AdminForm::form();
	$form->items([
		FormItem::text('title', 'Заголовок')->required(),
		FormItem::disabled('slug', 'Алиас'),
		FormItem::ckeditor('text', 'Текст')->required(),
		FormItem::image('image', 'Изображение')->required(),
		FormItem::checkbox('published', 'Опубликовано')->defaultValue(1),
	]);
	return $form;
});
