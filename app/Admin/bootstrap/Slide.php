<?php
use App\Models\Slide;

Admin::model(Slide::class)->title('Слайды')->display(function ()
{
	$display = AdminDisplay::table();
	$display->scope('ordered');
	$display->columns([
		Column::string('id')->label('ID'),
		Column::string('title')->label('Заголовок'),
		Column::sliderLink('link')->label('Ссылка'),
		Column::image('image')->label('Изображение'),
		Column::string('created_at')->label('Создан'),
		Column::order()->label('Порядок'),
		Column::publish('published')->label('Опубликовано'),
	]);
	return $display;
})->createAndEdit(function ()
{
	$form = AdminForm::form();
	$form->items([
		FormItem::text('title', 'Заголовок')->required(),
		FormItem::textaddon('link', 'Ссылка')->addon('http:'.url('').'/'),
		FormItem::ckeditor('text', 'Текст')->required(),
		FormItem::image('image', 'Изображение')->required(),
		FormItem::checkbox('published', 'Опубликовано')->defaultValue(1),
	]);
	return $form;
});
