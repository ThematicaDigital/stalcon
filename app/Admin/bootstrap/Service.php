<?php
use App\Models\Service;

Admin::model(Service::class)->title('Услуги')->display(function ()
{
	// $display = AdminDisplay::datatables();
	// $display->columns([
	// 	Column::string('id')->label('ID'),
	// 	Column::string('title')->label('Заголовок'),
	// 	Column::image('image')->label('Изображение'),
	// 	Column::string('created_at')->label('Создан'),
	// 	Column::publish('published')->label('Опубликовано'),
	// ]);

	$display = AdminDisplay::tree();

	$display->parentField('parent');
	$display->orderField('order');
	$display->rootParentId(0);

	return $display;
})->createAndEdit(function ()
{
	$form = AdminForm::form();
	$form->items([
		FormItem::text('title', 'Заголовок')->required(),
		FormItem::select('parent', 'Родитель')->model(Service::class)->display('title'),
		FormItem::disabled('slug', 'Алиас'),
		FormItem::ckeditor('description', 'Текст')->required(),
		FormItem::image('image', 'Изображение'),
		FormItem::checkbox('published', 'Опубликовано')->defaultValue(1),
	]);
	return $form;
});
