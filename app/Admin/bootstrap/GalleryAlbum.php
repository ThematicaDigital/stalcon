<?php
use App\Models\GalleryItem;
use App\Models\GalleryAlbum;

Admin::model(GalleryAlbum::class)->title('Альбомы')->display(function ()
{
	$display = AdminDisplay::datatables();
	$display->attributes([
		'ordering' => false,
		// 'stateSave' => false,
	]);
	$display->scope('ordered');

	$display->filters([
		Filter::related('id')->model(GalleryItem::class)->display('album'),
	]);

	$display->columns([
		Column::string('id')->label('ID'),
		Column::string('title')->label('Заголовок'),
		Column::image('cover')->label('Изображение')->orderable(false),
		Column::string('created_at')->label('Создан'),
		Column::publish('published')->label('Опубликовано'),
		Column::photosLink('id')->label('Фотографии')->orderable(false),
	]);
	return $display;
})->createAndEdit(function ()
{
	$form = AdminForm::form();
	$form->items([
		FormItem::text('title', 'Заголовок')->required(),
		FormItem::disabled('slug', 'Алиас'),
		FormItem::ckeditor('description', 'Описание')->required(),
		FormItem::image('cover', 'Изображение')->required(),
		FormItem::checkbox('published', 'Опубликовано')->defaultValue(1),
	]);
	return $form;
});
