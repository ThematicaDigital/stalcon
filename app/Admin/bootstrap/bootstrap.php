<?php

/*
 * Describe you custom displays, columns and form items here.
 *
 *		Display::register('customDisplay', '\Foo\Bar\MyCustomDisplay');
 *
 *		Column::register('customColumn', '\Foo\Bar\MyCustomColumn');
 *
 * 		FormItem::register('customElement', \Foo\Bar\MyCustomElement::class);
 *
 */

Column::register('publish', App\Admin\Columns\Publish::class);
Column::register('photosLink', App\Admin\Columns\PhotosLink::class);
Column::register('albumLink', App\Admin\Columns\AlbumLink::class);
Column::register('sliderLink', App\Admin\Columns\SliderLink::class);

FormItem::register('disabled', App\Admin\FormItems\Disabled::class);
