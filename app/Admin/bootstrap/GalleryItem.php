<?php
use App\Models\GalleryItem;
use App\Models\GalleryAlbum;

Admin::model(GalleryItem::class)->title('Фотографии')->display(function ()
{
	$display = AdminDisplay::datatables();

	$display->filters([
		Filter::related('album_id')->model(GalleryAlbum::class)->display('album'),
	]);

	$display->columns([
		Column::string('id')->label('ID'),
		Column::string('title')->label('Заголовок'),
		Column::albumLink('gallery.title')->label('Альбом'),
		Column::image('image')->label('Изображение'),
		Column::publish('published')->label('Опубликовано'),
	]);
	return $display;
})->createAndEdit(function ()
{
	$form = AdminForm::form();
	$form->items([
		FormItem::text('title', 'Заголовок')->required(),
		FormItem::select('album_id', 'Альбом')->model(GalleryAlbum::class)->display('title'),
		FormItem::image('image', 'Изображение')->required(),
		FormItem::checkbox('published', 'Опубликовано')->defaultValue(1),
	]);
	return $form;
});
