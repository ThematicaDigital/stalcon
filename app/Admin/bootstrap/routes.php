<?php

Route::get('', [
	'as' => 'admin.home',
	function ()
	{
		return redirect('/admin/news_items');
	}
]);
