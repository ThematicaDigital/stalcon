<?php
use SleepingOwl\AdminAuth\Entities\Administrator;

Admin::model(Administrator::class)->title('Администраторы')->display(function ()
{
	$display = AdminDisplay::table();
	$display->columns([
		Column::string('id')->label('ID'),
		Column::string('name')->label('Имя'),
		Column::string('username')->label('Логин'),
		Column::string('created_at')->label('Создан'),
	]);
	return $display;
})->createAndEdit(function ()
{
	$form = AdminForm::form();
	$form->items([
		FormItem::text('name', 'Имя')->required(),
		FormItem::text('username', 'Логин')->required()->unique(),
		FormItem::password('password', 'Пароль')->required(),
	]);
	return $form;
});
