<?php

use SleepingOwl\AdminAuth\Entities\Administrator;
use App\Models\Article;
use App\Models\Service;
use App\Models\NewsItem;
use App\Models\GalleryItem;
use App\Models\GalleryAlbum;
use App\Models\Page;
use App\Models\Slide;

Admin::menu()->url(url(''))->label('На сайт')->icon('fa-eye');

Admin::menu()->label('Настройки')->icon('fa-cogs')->items(function(){
    Admin::menu(Administrator::class)->label('Администраторы')->icon('fa-user');
});

Admin::menu(Slide::class)->label('Слайдер')->icon('fa-image');

Admin::menu()->label('Наши работы')->icon('fa-folder')->items(function(){
    Admin::menu(GalleryAlbum::class)->label('Альбомы')->icon('fa-folder-open');
    Admin::menu(GalleryItem::class)->label('Изображения')->icon('fa-image');
});

Admin::menu(Article::class)->label('Статьи')->icon('fa-file-text');
Admin::menu(NewsItem::class)->label('Новости')->icon('fa-file-text');
Admin::menu(Page::class)->label('Страницы')->icon('fa-file-text');
Admin::menu(Service::class)->label('Услуги')->icon('fa-file-text');
