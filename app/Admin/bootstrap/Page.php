<?php
use App\Models\Page;

Admin::model(Page::class)->title('Статические страницы')->display(function ()
{
	$display = AdminDisplay::table();
	$display->columns([
		Column::string('id')->label('ID'),
		Column::string('title')->label('Заголовок'),
		Column::image('image')->label('Изображение'),
		Column::string('created_at')->label('Создан'),
	]);
	return $display;
})->createAndEdit(function ($id)
{
	$form = AdminForm::form();
	$form->items([
		FormItem::text('title', 'Заголовок')->required(),
		FormItem::text('slug', 'Алиас'),
		FormItem::ckeditor('text', 'Текст')->required(),
		FormItem::image('image', 'Изображение'),
	]);
	return $form;
})->delete(null);
